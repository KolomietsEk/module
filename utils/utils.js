
const useAddToCart = () => {
    const cart = JSON.parse(localStorage.getItem('cart')) || []

    return (product) => {
        const foundIndex = cart.findIndex(({id}) => id === product.id) 
           
        if(foundIndex !== -1){
                cart[foundIndex].count +=1;
              } else {
                product.count = 1
                cart.push(product);
            }

            localStorage.setItem('cart', JSON.stringify(cart))
        }
    }

    export const addToCart = useAddToCart();