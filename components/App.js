/*import header from './Header.js';
import main from './Main.js';
import footer from './Footer.js'*/
import './Head.js';


class App {
    #appElement

    constructor() {
        this.init()
    }

    create(){
        this.#appElement = document.createElement('div')
        this.#appElement.classList.add('app')
        document.body.appendChild(this.#appElement)
    }

    getData() {
        fetch('data/data.json')
        .then((response)=> response.text()) 
        .then((data)=> {
            setTimeout(()=>{
                localStorage.setItem('dataSpa', data)
                this.render()
            },2000)
        }) 
    }

   async render() {

    const headerData = await import('./Header.js')
    const header = headerData.default
    const headerHTMLElement = header.create();
    this.#appElement.appendChild(headerHTMLElement);

    const mainData = await import('./Main.js')
    const main = mainData.default
    const mainHTMLElement = main.create();
    this.#appElement.appendChild(mainHTMLElement);

    const footerData = await import('./Footer.js')
    const footer = footerData.default
    const footerHTMLElement = footer.create();
    this.#appElement.appendChild(footerHTMLElement);

        /*import('./Header.js').then((headerData)=>{
            const header = headerData.default
            const headerHTMLElement = header.create();
            this.#appElement.appendChild(headerHTMLElement);

            import('./Main.js').then((mainData)=>{
                const main = mainData.default
                const mainHTMLElement = main.create();
                this.#appElement.appendChild(mainHTMLElement);

                import('./footer.js').then((footerData)=>{
                    const footer = footerData.default
                    const footerHTMLElement = footer.create();
                    this.#appElement.appendChild(footerHTMLElement);
                })

            })
        })*/

    
    }

    init(){
        this.create();
        this.getData();
    }
}

new App();