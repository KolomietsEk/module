import { getDataFromLocalStorage } from '../service/dataFromLocalStorageService.js'
import { CATALOG } from '../constants/constants.js'
import { addToCart } from '../utils/utils.js'

class Main {
    #mainElement;

    addEventLocationChange () {
        window.addEventListener('hashchange', () => {
            this.render(location.hash)
        })
    }

    create() {
        this.#mainElement = document.createElement('main');
        this.#mainElement.classList.add('main');
        
        
        this.render(location.hash)
        this.addEventLocationChange()
        return this.#mainElement;
    }

    render(hash) {
        if(location.pathname === "/index.html" || (location.pathname === "/" &&
        !location.hash)) {
            location.replace('/#home');
            return;
        }

        const slugFromHash = hash.slice(1);             
      
        const data = getDataFromLocalStorage()
        const mainData = data.find(({slug})=> slug === slugFromHash)
        
        if (slugFromHash === 'cart') {
            import('./Cart.js').then(responseCart => {
                const cartHTMLElement = responseCart.default.create()
                this.#mainElement.innerHTML = this.getHTMLTemplatePage( undefined, cartHTMLElement.outerHTML )

                const deleteButtons = this.#mainElement.querySelectorAll('.cart__delete__button');

                if(!deleteButtons){
                    return
                }


                deleteButtons.forEach((deleteButton) => {
                    deleteButton.addEventListener('click', (event)=>{
                        this.deleteCartProduct(+event.target.id);
                        this.render(location.hash);
                    })
                })


            })
            return
        }


        if (slugFromHash.includes(CATALOG)) {
            
            if(slugFromHash === CATALOG) {
                this.#mainElement.innerHTML = "<h2>Loading...</h2>"
                import('./Catalog.js').then((catalogResponse)=>{
                    const catalog = catalogResponse.default.init();
                    catalog.then((catalogHTMLElement)=>{
                        const catalogData = catalogResponse.default.catalogData
                        this.#mainElement.innerHTML= this.getHTMLTemplatePage (undefined, catalogHTMLElement.outerHTML)
                        const addButtons = this.#mainElement.querySelectorAll('.catalog__item__add__cart')
                        addButtons.forEach(addButtons => {
                            addButtons.addEventListener('click', (event)=>{
                                const product = catalogData.find(({id})=> id === +event.target.id)
                                addToCart(product) 
                            })
                        })
                
                    })
            
                })
                return;
            }

            this.#mainElement.innerHTML = "<h2>Loading...</h2>"
            import('./Product.js').then((responseProduct)=>{
                const product = responseProduct.default.init();
                product.then((productHTML)=>{
                    const productData = responseProduct.default.productData
                    this.#mainElement.innerHTML = productHTML.outerHTML;

                    const addButton = this.#mainElement.querySelector('.product__add__cart')

                    addButton.addEventListener('click', () => {
                        addToCart(productData)
                    })

                    const goBackBtn =  this.#mainElement.querySelector('.go__back__btn')
                    goBackBtn.addEventListener('click', ()=>{
                        location.replace(`#${CATALOG}`)
                    })
                })
                
            })
            return;
        }
        
        this.#mainElement.innerHTML= this.getHTMLTemplatePage(mainData.content)
    }

    deleteCartProduct (productId) {
        const cart = JSON.parse(localStorage.getItem('cart'))
        const filterCart = cart.filter(({id}) => id !== productId)
        localStorage.setItem('cart', JSON.stringify(filterCart))
    }

    getHTMLTemplatePage ( content, children) {
        return `<div class = "container">
                <div class = "main__wrapper">
                    ${children ? children : ""}
                    ${content ? content : '' }
                </div>                                            
            </div>`
    }
}

const main = new Main();

export default main;