import nav from './Nav.js'

class Header {
    create() {
        const header = document.createElement('header');
        const navHTMLElement = nav.create();
        header.classList.add('header');
        header.innerHTML=`<div class="container">
                            <div class="header__top">
                                <div class="header__logo">
                                    <a href="#" class="header__logo__link">
                                        <img src="../img/logo.png" alt="logo">
                                    </a>
                                </div> 
                    
                                     ${navHTMLElement.outerHTML}
                                
                                <div class="header__account__link">
                                    <a href="#" class="header__account">
                                        <img src="../img/cart.png">
                                    </a>
                                    <a href="#" class="header__cart">
                                        <img src="../img/profil.png">
                                    </a>
                                </div>
                            </div>
                            
                          </div>`
        return header
    }
}

const header = new Header();

export default header;