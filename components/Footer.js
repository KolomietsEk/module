class Footer {
    create() {
        const footerElement = document.createElement('footer');
        footerElement.classList.add('footer');
        footerElement.innerHTML=` <div class="container">
                                        <div class="footer__wrapper">
                                            <div class="footer__wrapper__contact">
                                                <h4>Contact Us</h4>
                                                <span class="footer__span">Email</span>
                                                <a href="info@cozy.by">info@cozy.by</a>
                                                <span class="footer__span">Phone</span>
                                                <a href="tel:+375293926999">375(29)392-69-99</a>
                                            </div>

                                            <div class="footer__wrapper__social">
                                                <a href="#" class="footer__logo__link">
                                                    <img src="../img/logo.png" alt="logo" alt="logo">
                                                </a>
                                                <span class="footer__span">COZY is a chain of tableware and household goods stores. The true value for us is the trust of customers. We offer only those products that we are confident in the quality of ourselves.</span>
                                                <div class="social">
                                                    <a href="#">
                                                        <img src="../img/face.png">
                                                    </a>
                                                    <a href="#">
                                                        <img src="../img/inst.png">
                                                    </a>
                                                    <a href="#">
                                                        <img src="../img/tg.png">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="footer__wrapper__utility">
                                                <h4>Information</h4>
                                                <a href="#">Акции</a>
                                                <a href="#">Блог</a>
                                                <a href="#">Новости</a>
                                                <a href="#">Договор оферты</a>
                                            </div>
                                        </div>
                                    </div>`
        return footerElement;
    }
}

const footer = new Footer();
export default footer;